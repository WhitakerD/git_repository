﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CrosshairLogic : MonoBehaviour {

    /// TO DO:
    /// - Find a way to prevent the mouse from going further than the crosshair
    /// OR
    /// - Create a "GUI Mouse" (No idea how this works)
    
    [SerializeField]
    private Vector3 crosshairPosition;
    private float currentDistance;

    [SerializeField]
    private float crosshairRange;

    private float distanceRangeProportion;

    [SerializeField]
    private Transform player;
    [SerializeField]
    private float sensitivity;

    
	
	// Update is called once per frame
	void Update () {

        // Sets the crosshair to a fraction of the vector between the player and the mouse and sets the elevation to ground level
        Vector3 moveDelta = new Vector2(Input.GetAxis("Mouse X"), Input.GetAxis("Mouse Y"));
        transform.position = transform.position + (moveDelta * sensitivity);

        // Keeps the crosshair from extending beyond its range.
        currentDistance = Vector3.Distance(player.position, transform.position);
        if (currentDistance > crosshairRange)
        {
            distanceRangeProportion = crosshairRange / currentDistance;
            transform.position = Vector3.Lerp(player.position, transform.position, distanceRangeProportion);
        }

        // Forces the player to face the crosshair
        player.LookAt(transform.position, Vector3.forward);  
    }
}