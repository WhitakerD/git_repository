﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraPositionComponent : MonoBehaviour {

    // This will be connected to the camera position that lies between the crosshair and the player.


    // Used to determine the appropriate zoom of the Camera
    [SerializeField]
    private float cameraSizeOriginal;
    [SerializeField]
    private float cameraSizeChange;
    private float cameraSizeNew;
    private float playerSimulatedCrosshairDistance;

    // Determines index of Player(Crosshair) for referencing in LERP equations.
    [SerializeField]
    private Transform crosshair;

    // Used for building a camera deadzone and a camera anchor weight to the player
    [SerializeField]
    private float cameraDeadzone;
    [SerializeField]
    private float cameraAnchor;

    private float playerCrosshairDistance;
    private float deadzoneDistanceProportion;

    private Vector3 simulatedCrosshairPosition;
    private Vector3 idealPosition;
    private Vector3 playerPosition;
    private Vector3 crosshairPosition;
    private Vector3 cameraPosition;


    // Use this for initialization
    void Start() {
    }

    // Update is called once per frame
    void Update () {

        // Referencing gameObjects needed for relevant positions - ie Player and Crosshair
        playerPosition = transform.parent.gameObject.transform.position;

        // Determines Player -> Crosshair Distance to determine whether to break out of deadzone
        playerCrosshairDistance = Vector3.Distance(playerPosition, crosshair.position);
        if (playerCrosshairDistance > cameraDeadzone)
        {
            // Simulates a crosshair position adjusted for the deadzone for a smooth camera transition
            deadzoneDistanceProportion = cameraDeadzone / playerCrosshairDistance;
            simulatedCrosshairPosition = Vector3.Lerp(crosshair.position, playerPosition, deadzoneDistanceProportion);

            // Calculates the realtime position of the camera
            idealPosition = Vector3.Lerp(playerPosition, simulatedCrosshairPosition, cameraAnchor);
            cameraPosition = idealPosition;

            // Determines the impending change on the camera size
            playerSimulatedCrosshairDistance = Vector3.Distance(playerPosition, simulatedCrosshairPosition);
            cameraSizeNew = cameraSizeOriginal + (cameraSizeChange * playerSimulatedCrosshairDistance);

        }
        else
        {
            // Resets to center on player with default camera size within Deadzone.
            cameraPosition = playerPosition;
            cameraSizeNew = cameraSizeOriginal;
            
        }

        // Sets the final attributes to the camera attributes
        transform.position = cameraPosition;
        cameraPosition.z = -10;
        Camera.main.transform.position = cameraPosition;
        Camera.main.orthographicSize = cameraSizeNew;

    }
}
